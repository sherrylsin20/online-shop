<?php

namespace App\Http\Livewire;
use DB;
use App\City;
use App\Province;
use App\Models\Order; 
use App\Models\Order_detail; 
use App\Models\Pengaturan_toko; 
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Kavist\RajaOngkir\RajaOngkir;

class Keranjang extends Component  
{
    public $order_id;
    public $order_status = 0;
    public function mount()
    {
        if(!Auth::user()){
            return redirect()->route('login');
        }
    }

    public function destroy($id)
    {
        DB::transaction(
            function() use($id) 
            { 
                $order_detail = Order_detail::find($id);
                if(!empty($order_detail))
                {
                    $order = Order::where('user_id',Auth::user()->id)->where('status',0)->first();
                    if($order->user_id != Auth::user()->id){
                        session()->flash('message','eits bukan hak anda');
                        return;
                    }
                    $jumlah_order_detail = Order_detail::where('order_id', $order->id)->count();
                    if($jumlah_order_detail == 1) {
                        $order->delete();
                    }
                    else{
                        $order->total_harga = $order->total_harga - $order_detail->jumlah_harga;
                        $order->update();
                    }
                    $order_detail->delete();
                }
            }
        );     
        $this->emit('notifKeranjang'); 
        session()->flash('message', 'Pesanan Dihapus');
    }
 
    public function render() 
    { 
        $order;
        $order_details = [];
        if(Auth::user()){
            $order = Order::where('user_id', Auth::user()->id)->where('status',$this->order_status)->first();
            if(!empty($order)){
                $order_details = Order_detail::where('order_id', $order->id)->get();
            }
        }
        else{
            return view('livewire.counter')
            ->extends('layouts.app') 
            ->section('content'); 
        }
        return view('livewire.keranjang',[
            'order' => $order,
            'order_details' => $order_details
        ])
        ->extends('layouts.app') 
        ->section('content');
    }
}
 