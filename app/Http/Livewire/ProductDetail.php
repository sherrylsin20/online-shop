<?php

namespace App\Http\Livewire;
use App\Models\Order;
use App\Models\Order_detail;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Kavist\RajaOngkir\RajaOngkir;
  
class ProductDetail extends Component
{
    public $product;
    public $jumlah_pesanan;
    public $is_available_ukuran = false, $is_available_warna = false, $is_available_varian_lainnya = false;
    public $ukuran,$warna,$varian_lainnya;
    public function mount($id) 
    {
        #mengambil data produk
        $productDetail  = Product::find($id);
        if(!empty($productDetail)){ 
            $this->product  = $productDetail;
        }
        else{
            return redirect()->to('');
        }

        #Varian Produk
        //warna
        $warna  = json_decode($productDetail->list_warna,true);
        if(count($warna) >= 1){
            if($warna[0] != 'null'){
                $this->is_available_warna = true;
            }
        }
        //varian_lainnya
        $varian_lainnya         = $productDetail->varian_lainnya;
        $list_varian_lainnya    = json_decode($productDetail->list_varian_lainnya,true);
        if($varian_lainnya != 'null' && count($list_varian_lainnya) >= 1){
            if($list_varian_lainnya[0] != 'null'){
                $this->is_available_varian_lainnya = true;
            }
        }
        //ukuran
        $ukuran  = json_decode($productDetail->list_ukuran,true);
        if(count($ukuran) >= 1){
            if($ukuran[0] != 'null'){
                $this->is_available_ukuran = true;
            }
        }
    }

    public function tambahKeranjang()
    {
        //validasi
        if(!Auth::user()){
            session()->flash('message','Anda harus login terlebih dahulu');
            return redirect()->route('login');
        }
        if($this->jumlah_pesanan < 1){
            session()->flash('message','Masukkan Jumlah Pesanan minimal 1');
            return;
        }
        //validasi varian produk
        if($this->is_available_ukuran && !$this->ukuran){
            session()->flash('message','silakan tambahkan ukuran produk');
            return;
        }
        if($this->is_available_warna && !$this->warna){
            session()->flash('message','silakan tambahkan warna produk');
            return;
        }
        if($this->is_available_varian_lainnya && !$this->varian_lainnya){
            session()->flash('message','silakan lengkapi dulu varian produk');
            return;
        }
        if($this->product->stok - $this->jumlah_pesanan < 0){
            session()->flash('message','stok produk telah habis');
            return;
        }
        $total_harga = $this->jumlah_pesanan * ($this->product->harga - (($this->product->harga *$this->product->diskon) / 100 ) );
        $order          = Order::where('user_id',Auth::user()->id)->where('status',0)->first();
        if(empty($order)){
            Order::create(
                [
                    'user_id'       => Auth::user()->id,
                    'total_harga'   => $total_harga,
                    'status'        => 0
                ]
            );
            $order          = Order::where('user_id',Auth::user()->id)->where('status',0)->first();
            $order->kode    = 'Belanja-'.$order->id."-".Auth::user()->id;
            $order->unik    = $order->id."-".mt_rand(1,9999)."-".Auth::user()->id;
            $order->update();
        }
        else{
            $order->total_harga = $order->total_harga + $total_harga;
            $order->update();
        }

        if(!empty($order)){
            //menambah varian
            $varian = '';
            if($this->ukuran){
                $varian .= $this->ukuran.", "; 
            }
            if($this->warna){
                $varian .= $this->warna.", "; 
            }
            if($this->varian_lainnya){
                $varian .= $this->varian_lainnya;
            }
            Order_detail::create( 
                [
                    'jumlah_pesanan'    => $this->jumlah_pesanan,
                    'jumlah_harga'      => $total_harga,
                    'varian'            => $varian,
                    'product_id'        => $this->product->id,
                    'order_id'          => $order->id
                ]
            );
        }
        $this->emit('notifKeranjang');
        session()->flash('message','Produk telah dimasukkan ke keranjang');  
    }
 
    public function render()
    {
        //false page
        if(!$this->product){
            return view('livewire.counter')
            ->extends('layouts.app') 
            ->section('content');
        }
        return view('livewire.product-detail',
        ['product'          => $this->product,
         'v_warna'          => json_decode($this->product->list_warna),
         'v_ukuran'         => json_decode($this->product->list_ukuran),
         'v_varian_lainnya' => json_decode($this->product->list_varian_lainnya)]
        )->extends('layouts.app')->section('content');;
    }
}
  