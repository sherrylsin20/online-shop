<?php

namespace App\Http\Livewire;
use DB;
use App\Models\Order;
use App\Models\Order_detail;
use App\Models\User;
use App\Models\Product;
use App\Models\Kategori;
use Illuminate\Support\Facades\Auth;
use Livewire\Component; 
  
class Navbar extends Component
{
    public $jumlah_keranjang = 0;
    protected $listeners = [
        'notifKeranjang' => 'updateKeranjang'
    ]; 
    public function updateKeranjang()
    {
        if(Auth::user()){ 
            $order = Order::where('status',0)->where('user_id', Auth::user()->id)->get();
            foreach ($order as $row){
               $this->jumlah_keranjang += Order_detail::where('order_id', $row->id)->count();
            }
        }
    }

    public function render()
    {
         if(Auth::user()){
            if(!$this->jumlah_keranjang){
                $order = Order::where('status',0)->where('user_id', Auth::user()->id)->get();
                foreach ($order as $row){
                   $this->jumlah_keranjang += Order_detail::where('order_id', $row->id)->count();
                }                              
            }
        }
        return view('livewire.navbar',[
            'Kategoris' => Kategori::all(),
            'jumlah_pesanan'    => $this->jumlah_keranjang 
        ])->extends('layouts.app')->section('content');     
    }
}
