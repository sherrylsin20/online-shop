<?php
namespace App\Http\Livewire; 
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
class Home extends Component   
{
    public function render()
    {        
        return view('livewire.home',['product_semua' => Product::skip(0)->take(8)->orderBy('id','asc')->get()])
        ->extends('layouts.app')->section('content');     
    }
}
 