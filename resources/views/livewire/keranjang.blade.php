<div class="container">
    <div class="row mt-4 mb-2">
        <div class="col">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('') }}" class="text-dark">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Keranjang</li>
                </ol>
            </nav>
        </div>
    </div>

  
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('message'))
            <div class="alert alert-warning">
                {{ session('message') }}
            </div>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="table-responsive"> 
                <table class="table text-center">
                    <thead>
                        <tr>
                            <td>No.</td>
                            <td>Gambar</td>
                            <td>Nama Produk</td>
                            <td>Jumlah</td>
                            <td>Harga</td> 
                            <td>Total Harga</td>
                            <td>Keterangan</td>
                            @if($order_status == 0)
                            <td>Hapus</td>
                            @endif
                        </tr>
                    </thead> 
                    <tbody> 
                        <?php $no = 1 ?>
                        @forelse ($order_details as $order_detail)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td>
                                <img src="{{ asset('storage/photos/'.$order_detail->product->gambar) }}" width="200px" height="225px">
                            </td>
                            <td>
                                {{ $order_detail->product->nama }}
                            </td> 
                            <td>{{ $order_detail->jumlah_pesanan }}</td>
                            <td>Rp. {{ number_format($order_detail->product->harga) }}</td>
                            <td><strong>Rp. {{ number_format($order_detail->jumlah_harga) }}</strong></td>
                            <td>
                                <strong>Total Harga sudah termasuk diskon {{$order_detail->diskon}}</strong>
                            </td>
                            @if($order_status == 0)
                            <td>    
                                <button class="btn btn-danger btn-block" wire:click="destroy({{ $order_detail->id }})" >
                                    Hapus
                                </button> 
                            </td>
                            @endif
                        </tr>    
                        @empty
                        <tr><td colspan="7">Data Kosong</td></tr>   
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>