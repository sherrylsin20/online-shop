<div class="container">

   <div class="banner">
      <img src="{{ asset('storage/photos/banner.jpg') }}" width="1100px" height="380px ">
   </div>

   <div class="row">
        <div class="col-md-12">
            @if(session()->has('message')) 
            <div class="alert alert-warning">
                {{ session('message') }}
            </div>
            @endif
        </div>
   </div>


   {{-- Semua Product  --}}
   <section class="products mt-5 mb-5">
      <div class="row mt-4">
         @foreach($product_semua as $product)
         <div class="col-md-3">
            <div class="card">
               <div class="card-body text-center">
               <div><h7>diskon {{$product->diskon}}% </h5></div>
               <img src="{{ asset('storage/photos/'.$product->gambar) }}" width="200px" height="270px">
                  <div class="row mt-2">
                     <div class="col-md-12">
                        <h5><strong>{{ $product->nama }}</strong> </h5>
                        <h6><strong>Rp. {{ number_format($product->harga) }}</strong></h6>
         
                     </div>
                  </div>
                  <div class="row mt-2">
                     <div class="col-md-12">
                        <a href="{{ url('ProductDetail/'.$product->id) }}" class="btn btn-success btn-block" style="background:#CBAACB"><i class="fas fa-eye"></i> Detail</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         @endforeach
      </div>
   </section>
</div>