<?php

use Illuminate\Support\Facades\Route;
use App\Http\Livewire\Keranjang;

 
/* 
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for  your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
 
Auth::routes();
Route::get('/', \App\Http\Livewire\Home::class);
Route::get('/ProductDetail/{id}', \App\Http\Livewire\ProductDetail::class);
Route::get('/Keranjang', \App\Http\Livewire\Keranjang::class)->name('keranjang');
Route::get('/Navbar', \App\Http\Livewire\Navbar::class);
